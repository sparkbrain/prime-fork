﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveRbody : MonoBehaviour
{
    Rigidbody m_rbody;
    
    public float m_movementSpeed = 1;
    public float m_downForce;

    public string m_horizontalAxisName;
    public string m_verticalAxisName;
    public bool m_invertHorizontalAxis;
    public bool m_invertVerticalAxis;

    public string m_sprintButtonName;
    public float m_sprintSpeedMultiplier;

    float m_xValue;
    float m_yValue;


    void Start()
    {
        m_rbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        //Get values from controller/keyboard and invert them as set in inspector
        m_xValue = Input.GetAxis(m_horizontalAxisName);
        m_yValue = Input.GetAxis(m_verticalAxisName);

        m_xValue = m_invertHorizontalAxis ? m_xValue * -1 : m_xValue;
        m_yValue = m_invertVerticalAxis ? m_yValue * -1 : m_yValue;

        MovePlayer();
    }

    //TODO: 
    //fix downward gravity. Currently always downForce or downforce * sprintMultiplier
    void MovePlayer()
    {
        Vector3 movedir = new Vector3(m_xValue, -m_downForce, m_yValue);
        movedir = transform.TransformDirection(movedir);

        Vector3 velocity = movedir * Time.deltaTime * (Input.GetButton(m_sprintButtonName) ? m_movementSpeed * m_sprintSpeedMultiplier : m_movementSpeed);

        m_rbody.velocity = velocity;
    }
}

